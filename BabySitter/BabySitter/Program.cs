﻿using System;
using System.Collections.Generic;

namespace BabySitter
{
    class Program
    {
        static void Main(string[] args)
        {
            bool proceed = false;
            bool startOver = false;
            string familyNameInput;
            BabySitterCalculator calculator = new BabySitterCalculator();
            do
            {
                do
                {
                    Console.WriteLine("Please enter the start hour. Example: 5pm");
                    string startInput = Console.ReadLine();
                    proceed = calculator.TraslateUserInputStringToHour(startInput, out int startHour);

                    if (!proceed)
                    {
                        Console.WriteLine(String.Format("{0} is not a valid input for start hour. Please try again.", startInput));
                        continue;
                    }

                    proceed = calculator.SetStartHour(startHour);
                    if (!proceed)
                        Console.WriteLine(String.Format("{0} is not a valid hour for start hour.", startInput));
                }
                while (proceed == false);

                do
                {
                    proceed = false;
                    Console.WriteLine("Please enter the end hour. Example: 5pm");
                    string endInput = Console.ReadLine();
                    proceed = calculator.TraslateUserInputStringToHour(endInput, out int endHour);

                    if (!proceed)
                    {
                        Console.WriteLine(String.Format("{0} is not a valid input for end hour.", endInput));
                        continue;
                    }

                    proceed = calculator.SetEndHour(endHour);
                    if (!proceed)
                        Console.WriteLine(String.Format("{0} is not a valid hour for end hour. End hour must be between 5pm-4am and be a whole hour increment", endInput));
                }
                while (proceed == false);

                startOver = calculator.ValidateStartEndRagnge();
                if (startOver == false)
                    Console.WriteLine("The enterd shift must have a starting hour that is before the end hour and be within business hours.");
            }
            while (startOver == false);

            do
            {
                proceed = false;
                Console.WriteLine("Please enter the name of the family you are sitting for. Example: A");
                familyNameInput = Console.ReadLine();

                switch (familyNameInput.ToUpper())
                {
                    case "A":
                        Dictionary<int, double> hourlyRatesA = new Dictionary<int, double>() { { 17, 15 }, { 18, 15 }, { 19, 15 }, { 20, 15 }, { 21, 15 }, { 22, 15 }, { 23, 20 }, { 0, 20 }, { 1, 20 }, { 2, 20 }, { 3, 20 } };
                        Family famA = new Family(hourlyRatesA, "A");
                        double totalPayA = calculator.CalculateTotalPay(famA);
                        Console.WriteLine(String.Format("Todays total comes to ${0}", totalPayA));
                        proceed = true;
                        break;

                    case "B":
                        Dictionary<int, double> hourlyRatesB = new Dictionary<int, double>() { { 5, 12 }, { 6, 12 }, { 7, 12 }, { 8, 12 }, { 9, 12 }, { 10, 8 }, { 11, 8 }, { 12, 8 }, { 1, 16 }, { 2, 16 }, { 3, 16 } };
                        Family famB = new Family(hourlyRatesB, "B");
                        double totalPayB = calculator.CalculateTotalPay(famB);
                        Console.WriteLine(String.Format("Todays total comes to ${0}", totalPayB));
                        proceed = true;
                        break;

                    case "C":
                        Dictionary<int, double> hourlyRatesC = new Dictionary<int, double>() { { 5, 21 }, { 6, 21 }, { 7, 21 }, { 8, 21 }, { 9, 15 }, { 10, 15 }, { 11, 15 }, { 12, 15 }, { 1, 15 }, { 2, 15 }, { 3, 15 } };
                        Family famC = new Family(hourlyRatesC, "C");
                        double totalPayC = calculator.CalculateTotalPay(famC);
                        Console.WriteLine(String.Format("Todays total comes to ${0}", totalPayC));
                        proceed = true;
                        break;
                    default:
                        proceed = false;
                        Console.WriteLine(String.Format("{0} is not a valid entry for family. Please enter family name.", familyNameInput));
                        break;
                }
            }
            while (proceed == false);
        }
    }
}

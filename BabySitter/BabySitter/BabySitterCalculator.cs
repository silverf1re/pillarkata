﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using BabySitter;

public class BabySitterCalculator
{
    public int StartHour { get; private set; }
    public int EndHour { get; private set; }
    public List<int> operatingHours { get; private set; }

    public BabySitterCalculator()
    {
        operatingHours = new List<int>() { 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4 };
    }

    public bool SetStartHour(int startHour)
    {
        if (ValidateHour(startHour))
        {
            StartHour = startHour;
            return true;
        }
        return false;
    }

    public bool SetEndHour(int endHour)
    {
        if (ValidateHour(endHour))
        {
            EndHour = endHour;
            return true;
        }
        return false;
    }

    private bool ValidateHour(int hour)
    {
        return ((hour >= 17 && hour <= 23) || (hour >= 0 && hour <= 4));
    }

    public bool TraslateUserInputStringToHour(string hourString, out int hour)
    {
        DateTime time = new DateTime();
        string[] dateTimeAcceptedFormats = { "hh:mmtt", "h:mmtt", "htt", "hhtt" };
        bool dateParseSucessful = DateTime.TryParseExact(hourString, dateTimeAcceptedFormats, new CultureInfo("en-US"), DateTimeStyles.AllowInnerWhite, out time);
        hour = time.Hour;
        if (time.Minute != 0)
            return false;
        return dateParseSucessful;
    }

    public bool ValidateStartEndRagnge()
    {
        int indexOfStartHour = this.operatingHours.IndexOf(StartHour);
        int indexOfEndHour = this.operatingHours.IndexOf(EndHour);
        return indexOfStartHour < indexOfEndHour;
    }

    public double CalculateTotalPay(Family family)
    {
        int startHourIndex = this.operatingHours.IndexOf(StartHour);
        int endHourIndex = this.operatingHours.IndexOf(EndHour);
        List<int> hoursWorked = this.operatingHours.GetRange(startHourIndex, endHourIndex - startHourIndex);
        double totalPay = 0;

        foreach (int hour in hoursWorked)
        {
            totalPay = totalPay + family.HourlyRates[hour];
        }

        return totalPay;
    }
}
﻿using System;
using System.Collections.Generic;

namespace BabySitter
{
    public class Family
    {
        public string Name { get; private set; }

        public IDictionary<int, double> HourlyRates { get; private set; }

        public Family(IDictionary<int, double> hourlyRates, string name)
        {
            HourlyRates = hourlyRates;
            Name = name;
        }
    }
}

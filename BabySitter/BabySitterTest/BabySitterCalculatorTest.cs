using System;
using System.Collections.Generic;
using System.Linq;
using BabySitter;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BabySitterTest
{
    [TestClass]
    public class BabySitterCalculatorTest
    {
        [TestMethod]
        public void whenStartTimeIs1StoreStartTime()
        {
            BabySitterCalculator calculator = new BabySitterCalculator();
            bool setStartBool = calculator.SetStartHour(1);
            Assert.IsTrue(setStartBool);
            Assert.IsTrue(calculator.StartHour == 1);
        }

        [TestMethod]
        public void whenStartTimeIs17AndEndTimeIs1StoreEndTime()
        {
            BabySitterCalculator calculator = new BabySitterCalculator();
            bool setStartBool = calculator.SetStartHour(17);
            bool setEndBool = calculator.SetEndHour(1);
            Assert.IsTrue(setStartBool);
            Assert.IsTrue(setEndBool);
            Assert.IsTrue(calculator.EndHour == 1);
        }

        [TestMethod]
        public void whenStartTimeIs17AndEndTimeIs23StoreEndTime()
        {
            BabySitterCalculator calculator = new BabySitterCalculator();
            bool setStartBool = calculator.SetStartHour(17);
            bool setEndBool = calculator.SetEndHour(23);
            Assert.IsTrue(setStartBool);
            Assert.IsTrue(setEndBool);
            Assert.IsTrue(calculator.EndHour == 23);
        }

        [TestMethod]
        public void whenStartTimeIs1AndEndTimeIs2StoreEndTime()
        {
            BabySitterCalculator calculator = new BabySitterCalculator();
            calculator.SetStartHour(1);
            calculator.SetEndHour(2);
            Assert.IsTrue(calculator.EndHour == 2);
        }

        [TestMethod]
        public void whenStartTimeIs7IsFalse()
        {
            BabySitterCalculator calculator = new BabySitterCalculator();
            bool setStartHour = calculator.SetStartHour(7);
            Assert.IsFalse(setStartHour);
        }

        [TestMethod]
        public void whenEndTimeIs11IsFalse()
        {
            BabySitterCalculator calculator = new BabySitterCalculator();
            bool setEndHour = calculator.SetEndHour(11);
            Assert.IsFalse(setEndHour);
        }

        [TestMethod]
        public void whenStartHourIs8AndEndHourIs7IsFalse()
        {
            BabySitterCalculator calculator = new BabySitterCalculator();
            bool setStartHourBool = calculator.SetStartHour(8);
            bool setEndHourBool = calculator.SetEndHour(7);
            Assert.IsFalse(calculator.ValidateStartEndRagnge());
        }

        [TestMethod]
        public void whenStartHourIs2AndEndHourIs7ReturnFalse()
        {
            BabySitterCalculator calculator = new BabySitterCalculator();
            bool setStartHourBool = calculator.SetStartHour(2);
            bool setEndHourBool = calculator.SetEndHour(7);
            Assert.IsFalse(calculator.ValidateStartEndRagnge());
        }

        [TestMethod]
        public void whenInitializeFamilyANameAndHourkyRateIsSet()
        {
            IDictionary<int, double> hourlyRates = new Dictionary<int, Double>() { { 5, 15 }, { 6, 15 }, { 7, 15 }, { 8, 15 }, { 9, 15 }, { 10, 15 }, { 11, 20 }, { 12, 20 }, { 1, 20 }, { 2, 20 }, { 3, 20 } };
            string name = "A";
            Family fam = new Family(hourlyRates, name);

            Assert.AreEqual(fam.HourlyRates, hourlyRates);
            Assert.AreEqual(fam.Name, name);
        }

        [TestMethod]
        public void whenStartAndEndHourAreTheSameReturnFalse()
        {
            BabySitterCalculator calculator = new BabySitterCalculator();
            bool setStartHourBool = calculator.SetStartHour(7);
            bool setEndHourBool = calculator.SetEndHour(7);
            Assert.IsFalse(calculator.ValidateStartEndRagnge());
        }

        [TestMethod]
        public void whenCalculatorGivenFamilAAnd17StartTimeAnd1EndTimeCalculatePayToBe130()
        {
            IDictionary<int, double> hourlyRates = new Dictionary<int, Double>() { { 17, 15 }, { 18, 15 }, { 19, 15 }, { 20, 15 }, { 21, 15 }, { 22, 15 }, { 23, 20 }, { 0, 20 }, { 1, 20 }, { 2, 20 }, { 3, 20 } };
            string name = "A";
            Family famA = new Family(hourlyRates, name);
            BabySitterCalculator calculator = new BabySitterCalculator();
            calculator.SetStartHour(17);
            calculator.SetEndHour(1);
            double totalPay = calculator.CalculateTotalPay(famA);
            Assert.IsTrue(totalPay == 130D);
        }

        [TestMethod]
        public void whenUserTypes5pmforHourInputTranslateItTo17()
        {
            BabySitterCalculator calculator = new BabySitterCalculator();
            int userHourInput; 
            calculator.TraslateUserInputStringToHour("5pm", out userHourInput);
            Assert.IsTrue(userHourInput == 17);
        }

        [TestMethod]
        public void whenUserTypes500pmforHourInputTranslateItTo5()
        {
            BabySitterCalculator calculator = new BabySitterCalculator();
            int userHourInput; 
            calculator.TraslateUserInputStringToHour("5:00pm", out userHourInput);
            Assert.IsTrue(userHourInput == 17);
        }

        [TestMethod]
        public void whenUserTypes5forHourInputRetunFalse()
        {
            BabySitterCalculator calculator = new BabySitterCalculator();
            int userHourInput; 
            bool validInput = calculator.TraslateUserInputStringToHour("5", out userHourInput);
            Assert.IsFalse(validInput);
        }

        [TestMethod]
        public void whenUserInputIs530pmReturnFalse()
        {
            BabySitterCalculator calculator = new BabySitterCalculator();
            int userHourInput;
            bool validInput = calculator.TraslateUserInputStringToHour("5:30pm", out userHourInput);
            Assert.IsFalse(validInput);
        }

        [TestMethod]
        public void whenUserStartInputIs1PmFalse()
        {
            BabySitterCalculator calculator = new BabySitterCalculator();
            int userHourInput;
            bool validInput = calculator.TraslateUserInputStringToHour("1pm", out userHourInput);
        }
    }
}
